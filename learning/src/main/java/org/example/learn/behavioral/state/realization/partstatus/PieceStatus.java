package org.example.learn.behavioral.state.realization.partstatus;

import org.example.learn.behavioral.state.realization.Part;

public class PieceStatus implements CreateStatus{
    @Override
    public void toNextStatus(Part part) {
        part.setStatus(new PickingStatus());
    }

    @Override
    public void toPrevStatus(Part part) {
        System.out.println("This is initial state");
    }

    @Override
    public void printStatus() {
        System.out.println("Status of the part: piece");
    }
}
