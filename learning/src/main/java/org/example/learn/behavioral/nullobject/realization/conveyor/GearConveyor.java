package org.example.learn.behavioral.nullobject.realization.conveyor;

import org.example.learn.behavioral.nullobject.realization.Conveyor;

public class GearConveyor implements Conveyor {
    @Override
    public void createPart() {
        System.out.println("Create gear part");
    }
}
