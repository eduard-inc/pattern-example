package org.example.learn.behavioral.interpreter.realization.expression;

public interface Expression {
    int interpretation();
}
