package org.example.learn.behavioral.state.realization;

import lombok.Setter;
import org.example.learn.behavioral.state.realization.partstatus.CreateStatus;
import org.example.learn.behavioral.state.realization.partstatus.PieceStatus;

@Setter
public class Part {

    private CreateStatus status = new PieceStatus();

    public void toPrevStatus() {
        status.toPrevStatus(this);
    }

    public void toNextStatus() {
        status.toNextStatus(this);
    }

    public void printStatus() {
        status.printStatus();
    }

}
