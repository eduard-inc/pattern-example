package org.example.learn.behavioral.memento.realization;

import lombok.AllArgsConstructor;
import lombok.Setter;

/**
 * Originator
 */

@AllArgsConstructor
@Setter
public class Recipe {
    private String salt;

    public Record save() {
        return new Record(salt);
    }

    public void recover(Record record) {
        this.salt = record.getSalt();
    }

    @Override
    public String toString() {
        return "Recipe{" +
                "salt=" + salt +
                '}';
    }
}
