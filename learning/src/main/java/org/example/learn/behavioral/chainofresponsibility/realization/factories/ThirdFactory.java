package org.example.learn.behavioral.chainofresponsibility.realization.factories;

import org.example.learn.behavioral.chainofresponsibility.realization.FactoryProcessor;
import org.example.learn.behavioral.chainofresponsibility.realization.Machine;
import org.example.learn.behavioral.chainofresponsibility.realization.machines.ThirdMachine;

public class ThirdFactory extends FactoryProcessor {
    public ThirdFactory(FactoryProcessor nextFactory) {
        super(nextFactory);
    }

    @Override
    public boolean createPart(Machine machine) {
        if (machine instanceof ThirdMachine) {
            System.out.println("Part " + machine.getClass().getSimpleName() + "was created by "
                    + this.getClass().getSimpleName());
            return true;
        } else if (nextFactory != null) {
            return nextFactory.createPart(machine);
        }
        return false;
    }
}
