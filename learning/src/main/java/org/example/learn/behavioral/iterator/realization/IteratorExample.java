package org.example.learn.behavioral.iterator.realization;

public class IteratorExample {

    private final String[] collection;
    private int cursor;

    public IteratorExample(String ... collection) {
        this.collection = collection;
    }

    public boolean hasNext(){
        return collection.length > cursor;
    }

    public String getNext(){
        return collection[cursor++];
    }
}
