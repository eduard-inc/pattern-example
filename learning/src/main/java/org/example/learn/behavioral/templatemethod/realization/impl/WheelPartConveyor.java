package org.example.learn.behavioral.templatemethod.realization.impl;

import org.example.learn.behavioral.templatemethod.realization.PartConveyor;

public class WheelPartConveyor extends PartConveyor {
    @Override
    protected void runConveyor() {
        System.out.println("Wheel conveyor was started");
    }

    @Override
    protected void createNewPart() {
        System.out.println("Wheel part was created");
    }

    @Override
    protected void stopConveyor() {
        System.out.println("Wheel conveyor was stopped");
    }
}
