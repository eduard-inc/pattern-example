package org.example.learn.behavioral.command.realization.invoker;

import org.example.learn.behavioral.command.realization.command.ConveyorAction;
import org.example.learn.behavioral.command.realization.command.DefaultConveyor;

import java.util.HashMap;
import java.util.Map;

public class ConveyorPanel {

    private final Map<String, ConveyorAction> conveyorActionList = new HashMap<>();

    public void register(String command, ConveyorAction conveyorAction) {
        conveyorActionList.put(command, conveyorAction);
    }

    public String execute(String command) {
        return conveyorActionList.computeIfAbsent(command, cmd -> new DefaultConveyor()).execute();
    }

}
