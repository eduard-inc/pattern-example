package org.example.learn.behavioral.command;

import org.example.learn.behavioral.command.realization.command.StartConveyor;
import org.example.learn.behavioral.command.realization.command.StopConveyor;
import org.example.learn.behavioral.command.realization.invoker.ConveyorPanel;
import org.example.learn.behavioral.command.realization.receiver.Conveyor;

/**
 * Паттерн Command используется для инкапсуляции всей информации для выполнения.
 */
public class CommandMain {
    public static void main(String[] args) {
        ConveyorPanel conveyorPanel = new ConveyorPanel();
        Conveyor conveyor = new Conveyor();

        conveyorPanel.register("on", new StartConveyor(conveyor));
        conveyorPanel.register("off", new StopConveyor(conveyor));

        System.out.println(conveyorPanel.execute("on"));
        System.out.println(conveyorPanel.execute("off"));
    }
}
