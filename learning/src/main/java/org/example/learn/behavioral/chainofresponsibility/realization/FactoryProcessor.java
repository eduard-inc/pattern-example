package org.example.learn.behavioral.chainofresponsibility.realization;

public abstract class FactoryProcessor {

    public FactoryProcessor nextFactory;

    public FactoryProcessor(FactoryProcessor nextFactory){
        this.nextFactory = nextFactory;
    }

    public abstract boolean createPart(Machine machine);

}
