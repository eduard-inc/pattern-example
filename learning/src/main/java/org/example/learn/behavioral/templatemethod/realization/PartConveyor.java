package org.example.learn.behavioral.templatemethod.realization;

public abstract class PartConveyor {

    protected abstract void runConveyor();

    protected abstract void createNewPart();

    protected abstract void stopConveyor();

    public final void createPart() {
        runConveyor();
        createNewPart();
        stopConveyor();

        System.out.println("Part is created");
    }

}
