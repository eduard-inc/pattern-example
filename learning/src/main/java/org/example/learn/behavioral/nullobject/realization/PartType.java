package org.example.learn.behavioral.nullobject.realization;

public enum PartType {

    WHEEL,
    GEAR,
    NULL

}
