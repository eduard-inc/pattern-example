package org.example.learn.behavioral.memento.realization;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Memento
 */
@AllArgsConstructor
@Getter
public class Record {
    private String salt;
}
