package org.example.learn.behavioral.visitor.realization;

public interface Part {
    void accept(Inspector inspector);
}
