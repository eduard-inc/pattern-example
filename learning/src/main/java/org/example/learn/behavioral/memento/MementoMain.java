package org.example.learn.behavioral.memento;

import org.example.learn.behavioral.memento.realization.Cookbook;
import org.example.learn.behavioral.memento.realization.Recipe;

public class MementoMain {
    public static void main(String[] args) {
        Recipe recipe = new Recipe("5g");
        System.out.println(recipe);

        Cookbook cookbook = new Cookbook();
        cookbook.setRecord(recipe.save());

        recipe.setSalt("10g");
        System.out.println(recipe);

        System.out.println("--");
        recipe.recover(cookbook.getRecord());
        System.out.println(recipe);

    }
}
