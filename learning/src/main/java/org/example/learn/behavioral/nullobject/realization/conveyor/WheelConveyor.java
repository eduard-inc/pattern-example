package org.example.learn.behavioral.nullobject.realization.conveyor;

import org.example.learn.behavioral.nullobject.realization.Conveyor;

public class WheelConveyor implements Conveyor {
    @Override
    public void createPart() {
        System.out.println("Create wheel part");
    }
}
