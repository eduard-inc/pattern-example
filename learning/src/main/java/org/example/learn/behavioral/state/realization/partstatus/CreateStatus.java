package org.example.learn.behavioral.state.realization.partstatus;

import org.example.learn.behavioral.state.realization.Part;

public interface CreateStatus {
    void toNextStatus(Part part);
    void toPrevStatus(Part part);
    void printStatus();
}
