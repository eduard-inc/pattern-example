package org.example.learn.behavioral.memento.realization;

import lombok.Getter;
import lombok.Setter;

/**
 * Caretaker
 */
@Getter
@Setter
public class Cookbook {

    private Record record;

}
