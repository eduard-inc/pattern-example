package org.example.learn.behavioral.visitor.realization;

import org.example.learn.behavioral.visitor.realization.part.GearPart;
import org.example.learn.behavioral.visitor.realization.part.WheelPart;

public interface Inspector {
    void visit(GearPart gear);
    void visit(WheelPart gear);
}
