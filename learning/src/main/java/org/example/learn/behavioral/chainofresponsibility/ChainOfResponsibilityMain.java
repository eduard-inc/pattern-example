package org.example.learn.behavioral.chainofresponsibility;

import org.example.learn.behavioral.chainofresponsibility.realization.FactoryProcessor;
import org.example.learn.behavioral.chainofresponsibility.realization.factories.FirstFactory;
import org.example.learn.behavioral.chainofresponsibility.realization.factories.SecondFactory;
import org.example.learn.behavioral.chainofresponsibility.realization.factories.ThirdFactory;
import org.example.learn.behavioral.chainofresponsibility.realization.machines.FirstMachine;
import org.example.learn.behavioral.chainofresponsibility.realization.machines.SecondMachine;
import org.example.learn.behavioral.chainofresponsibility.realization.machines.ThirdMachine;

/**
 *  Паттерн Chain of responsibility, создает цепочку отвественных объектов обработки.
 *  Где выполняется последовательный вызов комманд
 */
public class ChainOfResponsibilityMain {
    public static void main(String[] args) {

        FactoryProcessor thirdFactory = new ThirdFactory(null);
        FactoryProcessor secondFactory = new SecondFactory(thirdFactory);
        FactoryProcessor firstFactory = new FirstFactory(secondFactory);


        result(firstFactory.createPart(new FirstMachine()));
        result(firstFactory.createPart(new SecondMachine()));
        result(firstFactory.createPart(new ThirdMachine()));
        result(firstFactory.createPart(null));

    }

    private static void result(boolean part){
        if (!part) System.out.println("Part dont created");
    }
}
