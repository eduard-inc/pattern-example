package org.example.learn.behavioral.mediator;

import org.example.learn.behavioral.mediator.realization.Executor;
import org.example.learn.behavioral.mediator.realization.MediatorController;
import org.example.learn.behavioral.mediator.realization.realization.ControllerExample;
import org.example.learn.behavioral.mediator.realization.realization.WorkExecutorFirst;
import org.example.learn.behavioral.mediator.realization.realization.WorkExecutorSecond;

/**
 * Паттерн Mediator создает класс между объектами, который выступает в посредника между ними,
 * снижающий связанность между объектами.
 */
public class MediatorMain {
    public static void main(String[] args) {
        MediatorController mediatorController = new ControllerExample();

        Executor executorFirst = new WorkExecutorFirst(mediatorController);
        Executor executorSecond = new WorkExecutorSecond(mediatorController);

        mediatorController.addExecutor(executorFirst);
        mediatorController.addExecutor(executorSecond);

        executorFirst.send("task 1");
        executorSecond.send("task 2");
    }
}
