package org.example.learn.behavioral.interpreter.realization.expression;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class PlusExpression implements Expression {

    Expression left;
    Expression right;

    @Override
    public int interpretation() {
        return left.interpretation() + right.interpretation();
    }
}
