package org.example.learn.behavioral.interpreter.realization;

import org.example.learn.behavioral.interpreter.realization.expression.Expression;
import org.example.learn.behavioral.interpreter.realization.expression.MinusExpression;
import org.example.learn.behavioral.interpreter.realization.expression.NumberExpression;
import org.example.learn.behavioral.interpreter.realization.expression.PlusExpression;

public class ContextExpression {

    public Expression evaluate(String expression) {
        int position = expression.length() - 1;
        while (position > 0) {
            if (Character.isDigit(expression.charAt(position))) {
                position--;
            } else {
                Expression left = evaluate(expression.substring(0, position));
                Expression right = new NumberExpression(Integer.parseInt(expression.substring(position + 1)));
                char operator = expression.charAt(position);
                switch (operator) {
                    case '-':
                        return new MinusExpression(left, right);
                    case '+':
                        return new PlusExpression(left, right);
                }
            }
        }
        int result = Integer.parseInt(expression);
        return new NumberExpression(result);
    }

}
