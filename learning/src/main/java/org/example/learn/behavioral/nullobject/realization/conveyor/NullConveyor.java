package org.example.learn.behavioral.nullobject.realization.conveyor;

import org.example.learn.behavioral.nullobject.realization.Conveyor;

public class NullConveyor implements Conveyor {
    @Override
    public void createPart() {
        System.out.println("Dont create part");
    }
}
