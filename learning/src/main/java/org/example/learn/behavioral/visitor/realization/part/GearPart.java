package org.example.learn.behavioral.visitor.realization.part;

import lombok.Getter;
import org.example.learn.behavioral.visitor.realization.Part;
import org.example.learn.behavioral.visitor.realization.Inspector;

@Getter
public class GearPart implements Part {

    private final String name = "gear";

    @Override
    public void accept(Inspector inspector) {
        inspector.visit(this);
    }
}
