package org.example.learn.behavioral.state;

import org.example.learn.behavioral.state.realization.Part;

/**
 *
 */
public class StateMain {
    public static void main(String[] args) {

        Part part = new Part();
        part.printStatus();

        part.toNextStatus();
        part.printStatus();

        part.toNextStatus();
        part.printStatus();

        part.toNextStatus();
        part.printStatus();
    }
}
