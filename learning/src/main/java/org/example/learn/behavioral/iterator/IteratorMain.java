package org.example.learn.behavioral.iterator;

import org.example.learn.behavioral.iterator.realization.IteratorExample;

/**
 * Паттерн Iterator позволяет последовательно получить доступ ко всем объектом составног объекта
 */

public class IteratorMain {
    public static void main(String[] args) {
        IteratorExample iterator = new IteratorExample("one", "two", "three");

        while (iterator.hasNext()){
            System.out.println(iterator.getNext());
        }
    }
}
