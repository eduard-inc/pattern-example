package org.example.learn.behavioral.mediator.realization.realization;

import org.example.learn.behavioral.mediator.realization.Executor;
import org.example.learn.behavioral.mediator.realization.MediatorController;

public class WorkExecutorFirst extends Executor {
    public WorkExecutorFirst(MediatorController mediatorController) {
        super(mediatorController);
    }
}
