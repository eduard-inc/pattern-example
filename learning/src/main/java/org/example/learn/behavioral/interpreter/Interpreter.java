package org.example.learn.behavioral.interpreter;

import org.example.learn.behavioral.interpreter.realization.ContextExpression;

public class Interpreter {
    public static void main(String[] args) {

        ContextExpression context = new ContextExpression();
        System.out.println(context.evaluate("1+1+1").interpretation());
        System.out.println(context.evaluate("1-2+1").interpretation());
        System.out.println(context.evaluate("20+9-11").interpretation());

    }
}
