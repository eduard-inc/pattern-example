package org.example.learn.behavioral.observer.realiztion;

public interface AbstractPanel {

    void addInspector(Inspector inspector);

    void removeInspector(Inspector inspector);

    void notifyInspectors(String info);

}
