package org.example.learn.behavioral.nullobject.realization;

import org.example.learn.behavioral.nullobject.realization.conveyor.GearConveyor;
import org.example.learn.behavioral.nullobject.realization.conveyor.NullConveyor;
import org.example.learn.behavioral.nullobject.realization.conveyor.WheelConveyor;

public class PartFactory {

    public Conveyor getConveyor(PartType type) {
        switch (type) {
            case WHEEL:
                return new WheelConveyor();
            case GEAR:
                return new GearConveyor();
            default:
                return new NullConveyor();
        }
    }
}
