package org.example.learn.behavioral.nullobject;

import org.example.learn.behavioral.nullobject.realization.Conveyor;
import org.example.learn.behavioral.nullobject.realization.PartFactory;
import org.example.learn.behavioral.nullobject.realization.PartType;

/**
 *  Паттер Null Object создает реализацию по умолчанию, убирая необходимость проверки на null,
 *  что улучшает читаемость и последовательность кода
 */
public class NullObjectMain {
    public static void main(String[] args) {

        PartFactory factory = new PartFactory();
        Conveyor conveyor;

        conveyor = factory.getConveyor(PartType.WHEEL);
        conveyor.createPart();

        conveyor = factory.getConveyor(PartType.GEAR);
        conveyor.createPart();

        conveyor = factory.getConveyor(PartType.NULL);
        conveyor.createPart();

    }
}
