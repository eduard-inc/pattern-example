package org.example.learn.behavioral.strategy;

import org.example.learn.behavioral.strategy.realization.PartConveyor;
import org.example.learn.behavioral.strategy.realization.parttypes.Gear;
import org.example.learn.behavioral.strategy.realization.parttypes.Wheel;

/**
 * Паттерн Strategy позволяет выбирать текущий способ реализации в зависимости от контекста
 */
public class StrategyMain {
    public static void main(String[] args) {
        PartConveyor partConveyor = new PartConveyor();

        partConveyor.setPartType(new Gear());
        partConveyor.createPart();

        partConveyor.setPartType(new Wheel());
        partConveyor.createPart();

    }
}
