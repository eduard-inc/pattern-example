package org.example.learn.behavioral.mediator.realization;

public abstract class Executor {

    private final MediatorController mediatorController;

    public Executor(MediatorController mediatorController) {
        this.mediatorController = mediatorController;
    }

    public void send(String task) {
        mediatorController.sendTask(task, this);
    }

    public void notify(String message) {
        System.out.println(getClass().getSimpleName() + " received task:  " + message);
    }

}
