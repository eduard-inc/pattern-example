package org.example.learn.behavioral.command.realization.command;

@FunctionalInterface
public interface ConveyorAction {
    String execute();
}
