package org.example.learn.behavioral.templatemethod;

import org.example.learn.behavioral.templatemethod.realization.PartConveyor;
import org.example.learn.behavioral.templatemethod.realization.impl.GearPartConveyor;
import org.example.learn.behavioral.templatemethod.realization.impl.WheelPartConveyor;

/**
 * Паттерн Template method определяет задает структуру класса, для переопределения наследниками
 */
public class TemplateMethodMain {
    public static void main(String[] args) {
        PartConveyor gearPartConveyor = new GearPartConveyor();
        gearPartConveyor.createPart();

        PartConveyor wheelPartConveyor = new WheelPartConveyor();
        wheelPartConveyor.createPart();
    }
}
