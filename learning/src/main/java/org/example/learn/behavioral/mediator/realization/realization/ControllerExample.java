package org.example.learn.behavioral.mediator.realization.realization;

import lombok.Setter;
import org.example.learn.behavioral.mediator.realization.Executor;
import org.example.learn.behavioral.mediator.realization.MediatorController;

import java.util.HashSet;
import java.util.Set;

@Setter
public class ControllerExample implements MediatorController {

    Set<Executor> executors = new HashSet<>();

    public void addExecutor(Executor executor) {
        executors.add(executor);
    }

    @Override
    public void sendTask(String message, Executor sender) {
        executors.stream()
                .filter(executor -> !executor.equals(sender))
                .forEach(executor -> executor.notify(message));
    }
}
