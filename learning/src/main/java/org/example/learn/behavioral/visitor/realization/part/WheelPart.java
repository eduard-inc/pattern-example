package org.example.learn.behavioral.visitor.realization.part;

import lombok.Getter;
import org.example.learn.behavioral.visitor.realization.Part;
import org.example.learn.behavioral.visitor.realization.Inspector;

@Getter
public class WheelPart implements Part {

    private final String name = "wheel";

    @Override
    public void accept(Inspector inspector) {
        inspector.visit(this);
    }
}
