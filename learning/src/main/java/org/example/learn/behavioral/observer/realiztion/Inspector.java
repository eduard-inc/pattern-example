package org.example.learn.behavioral.observer.realiztion;

public interface Inspector {
    void receiveInfo(String info);
}
