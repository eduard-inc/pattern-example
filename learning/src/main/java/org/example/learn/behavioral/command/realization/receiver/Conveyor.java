package org.example.learn.behavioral.command.realization.receiver;

public class Conveyor {

    public String startConveyor() {
        return "Conveyor was started";
    }

    public String stopConveyor() {
        return "Conveyor was stopped";
    }

}
