package org.example.learn.behavioral.visitor;

import org.example.learn.behavioral.visitor.realization.Inspector;
import org.example.learn.behavioral.visitor.realization.Part;
import org.example.learn.behavioral.visitor.realization.inspector.Overseer;
import org.example.learn.behavioral.visitor.realization.inspector.Supervisor;
import org.example.learn.behavioral.visitor.realization.part.GearPart;
import org.example.learn.behavioral.visitor.realization.part.WheelPart;

/**
 * Шаблон Visitor позволяет классу посетителю расширить работу с классом наблюдателя, не прибегая к изменению самого
 * класса. Снижая связность.
 */
public class VisitorMain {
    public static void main(String[] args) {

        Inspector supervisor = new Supervisor();
        Inspector overseer = new Overseer();

        Part gear = new GearPart();
        Part wheel = new WheelPart();

        gear.accept(supervisor);
        gear.accept(overseer);

        wheel.accept(supervisor);
        wheel.accept(overseer);
    }
}
