package org.example.learn.behavioral.strategy.realization;

import lombok.Setter;

@Setter
public class PartConveyor {

    private PartPattern partType;

    public void createPart() {
        partType.createPart();
    }

}
