package org.example.learn.behavioral.command.realization.command;

import lombok.AllArgsConstructor;
import org.example.learn.behavioral.command.realization.receiver.Conveyor;

@AllArgsConstructor
public class StartConveyor implements ConveyorAction {

    private final Conveyor conveyor;
    @Override
    public String execute() {
        System.out.println("Execute " + this.getClass().getSimpleName());
        return conveyor.startConveyor();
    }
}
