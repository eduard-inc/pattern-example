package org.example.learn.behavioral.templatemethod.realization.impl;

import org.example.learn.behavioral.templatemethod.realization.PartConveyor;

public class GearPartConveyor extends PartConveyor {
    @Override
    protected void runConveyor() {
        System.out.println("Gear conveyor was started");
    }

    @Override
    protected void createNewPart() {
        System.out.println("Gear part was created");
    }

    @Override
    protected void stopConveyor() {
        System.out.println("Gear conveyor was stopped");
    }
}
