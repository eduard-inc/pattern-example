package org.example.learn.behavioral.visitor.realization.inspector;

import org.example.learn.behavioral.visitor.realization.Inspector;
import org.example.learn.behavioral.visitor.realization.part.GearPart;
import org.example.learn.behavioral.visitor.realization.part.WheelPart;

public class Overseer implements Inspector {
    @Override
    public void visit(GearPart part) {
        System.out.println(getClass().getSimpleName() + " inspect " + part.getName());
    }

    @Override
    public void visit(WheelPart part) {
        System.out.println(getClass().getSimpleName() + " inspect " + part.getName());
    }
}
