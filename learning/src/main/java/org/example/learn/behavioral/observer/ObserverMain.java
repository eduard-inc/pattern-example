package org.example.learn.behavioral.observer;

import org.example.learn.behavioral.observer.realiztion.AbstractPanel;
import org.example.learn.behavioral.observer.realiztion.Inspector;
import org.example.learn.behavioral.observer.realiztion.impl.ControlPanel;
import org.example.learn.behavioral.observer.realiztion.impl.MainInspector;
import org.example.learn.behavioral.observer.realiztion.impl.Supervisor;

/**
 *
 */
public class ObserverMain {
    public static void main(String[] args) {
        AbstractPanel panel = new ControlPanel();
        Inspector supervisor = new Supervisor();
        Inspector mainInspector = new MainInspector();

        panel.notifyInspectors("test 1");

        panel.addInspector(supervisor);
        panel.notifyInspectors("test 2");

        panel.addInspector(mainInspector);
        panel.notifyInspectors("test 3");

        panel.removeInspector(supervisor);
        panel.notifyInspectors("test 4");
    }
}
