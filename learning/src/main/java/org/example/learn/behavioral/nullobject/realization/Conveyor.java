package org.example.learn.behavioral.nullobject.realization;


public interface Conveyor {

    void createPart();

}
