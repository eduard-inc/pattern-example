package org.example.learn.behavioral.mediator.realization.realization;

import org.example.learn.behavioral.mediator.realization.Executor;
import org.example.learn.behavioral.mediator.realization.MediatorController;

public class WorkExecutorSecond extends Executor {
    public WorkExecutorSecond(MediatorController mediatorController) {
        super(mediatorController);
    }
}
