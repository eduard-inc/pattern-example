package org.example.learn.behavioral.mediator.realization;

public interface MediatorController {

    void addExecutor(Executor executor);

    void sendTask(String message, Executor sender);
}
