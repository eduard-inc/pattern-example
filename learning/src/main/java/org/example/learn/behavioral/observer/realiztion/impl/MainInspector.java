package org.example.learn.behavioral.observer.realiztion.impl;

import org.example.learn.behavioral.observer.realiztion.Inspector;

public class MainInspector implements Inspector {
    @Override
    public void receiveInfo(String info) {
        System.out.println(getClass().getSimpleName() + " got the info: " + info);
    }
}
