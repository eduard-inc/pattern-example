package org.example.learn.behavioral.strategy.realization;

public interface PartPattern {
    void createPart();
}
