package org.example.learn.behavioral.strategy.realization.parttypes;

import org.example.learn.behavioral.strategy.realization.PartPattern;

public class Gear implements PartPattern {
    @Override
    public void createPart() {
        System.out.println("Create part: gear");
    }
}
