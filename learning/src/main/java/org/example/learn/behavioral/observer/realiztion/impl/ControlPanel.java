package org.example.learn.behavioral.observer.realiztion.impl;

import org.example.learn.behavioral.observer.realiztion.AbstractPanel;
import org.example.learn.behavioral.observer.realiztion.Inspector;

import java.util.ArrayList;
import java.util.List;

public class ControlPanel implements AbstractPanel {

    List<Inspector> inspectorList = new ArrayList<>();

    public void addInspector(Inspector inspector) {
        inspectorList.add(inspector);
    }

    public void removeInspector(Inspector inspector) {
        inspectorList.remove(inspector);
    }

    public void notifyInspectors(String info) {
        inspectorList.forEach(inspector -> inspector.receiveInfo(info));
    }

}
