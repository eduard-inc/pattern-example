package org.example.learn.behavioral.interpreter.realization.expression;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class NumberExpression implements Expression {

    private int number;

    @Override
    public int interpretation() {
        return number;
    }
}
