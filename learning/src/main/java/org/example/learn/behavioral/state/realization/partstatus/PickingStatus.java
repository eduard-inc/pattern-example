package org.example.learn.behavioral.state.realization.partstatus;

import org.example.learn.behavioral.state.realization.Part;

public class PickingStatus implements CreateStatus {
    @Override
    public void toNextStatus(Part part) {
        part.setStatus(new CompletedStatus());
    }

    @Override
    public void toPrevStatus(Part part) {
        part.setStatus(new PieceStatus());
    }

    @Override
    public void printStatus() {
        System.out.println("Status of the part: picking");
    }
}
