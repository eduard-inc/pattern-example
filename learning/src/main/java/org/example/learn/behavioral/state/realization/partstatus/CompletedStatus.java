package org.example.learn.behavioral.state.realization.partstatus;

import org.example.learn.behavioral.state.realization.Part;

public class CompletedStatus implements CreateStatus {
    @Override
    public void toNextStatus(Part part) {
        System.out.println("This is finish state");
    }

    @Override
    public void toPrevStatus(Part part) {
        part.setStatus(new PickingStatus());
    }

    @Override
    public void printStatus() {
        System.out.println("Status of the part: complete");
    }
}
