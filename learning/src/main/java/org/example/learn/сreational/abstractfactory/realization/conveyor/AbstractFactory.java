package org.example.learn.сreational.abstractfactory.realization.conveyor;

import org.example.learn.сreational.abstractfactory.realization.product.Drink;
import org.example.learn.сreational.abstractfactory.realization.product.EnergyDrink;

public interface AbstractFactory {
    Drink createDrink();

    EnergyDrink createEnergyDrink();
}
