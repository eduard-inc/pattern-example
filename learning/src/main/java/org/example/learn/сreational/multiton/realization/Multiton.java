package org.example.learn.сreational.multiton.realization;

import java.util.HashMap;

public class Multiton {

    private final HashMap<FactoryTypeEnum, Factory> instances = new HashMap<>();

    public Factory getFactory(FactoryTypeEnum type) {
        System.out.println("Execute factory with type: " + type);
        return instances.computeIfAbsent(type, Factory::new);
    }

}
