package org.example.learn.сreational.objectpool;

import org.example.learn.сreational.objectpool.realization.Bottle;
import org.example.learn.сreational.objectpool.realization.ObjectPool;

/**
 * Шаблон Object pool создает и хранит созданные экземляры класса в пуле, и после их использования помещает их
 * обратно в пул.
 *
 *  Может повысить производительность, в случае частой генерации инстансов объекта
 */
public class ObjectPoolMain {
    public static void main(String[] args) {
        ObjectPool pool = new ObjectPool();

        Bottle bottle1 = pool.get();
        Bottle bottle2 = pool.get();
        pool.remove(bottle1);

        bottle1 = pool.get();
    }
}
