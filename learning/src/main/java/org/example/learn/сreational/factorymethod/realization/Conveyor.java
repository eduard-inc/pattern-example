package org.example.learn.сreational.factorymethod.realization;

public abstract class Conveyor {

    public abstract Drink createDrink();

}
