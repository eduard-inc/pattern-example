package org.example.learn.сreational.builder.realization;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class Drink {

    private String title;
    private String producer;
    private Double volume;

    public static Drink.DrinkBuilder builder() {
        return new Drink.DrinkBuilder();
    }

    public static class DrinkBuilder {
        private String title;
        private String producer;
        private Double volume;

        public Drink.DrinkBuilder title(final String title) {
            this.title = title;
            return this;
        }

        public Drink.DrinkBuilder producer(final String producer) {
            this.producer = producer;
            return this;
        }

        public Drink.DrinkBuilder volume(final Double volume) {
            this.volume = volume;
            return this;
        }

        public Drink build() {
            return new Drink(this.title, this.producer, this.volume);
        }
    }

}
