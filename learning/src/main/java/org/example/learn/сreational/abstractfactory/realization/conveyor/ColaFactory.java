package org.example.learn.сreational.abstractfactory.realization.conveyor;

import org.example.learn.сreational.abstractfactory.realization.product.Drink;
import org.example.learn.сreational.abstractfactory.realization.product.EnergyDrink;
import org.example.learn.сreational.abstractfactory.realization.product.impl.Cola;
import org.example.learn.сreational.abstractfactory.realization.product.impl.ColaEnergy;

public class ColaFactory implements AbstractFactory {
    @Override
    public Drink createDrink() {
        System.out.println("Create new drink: Cola");
        return new Cola();
    }

    @Override
    public EnergyDrink createEnergyDrink() {
        System.out.println("Create new drink: ColaEnergy");
        return new ColaEnergy();
    }
}
