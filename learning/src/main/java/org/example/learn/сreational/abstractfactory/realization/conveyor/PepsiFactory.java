package org.example.learn.сreational.abstractfactory.realization.conveyor;

import org.example.learn.сreational.abstractfactory.realization.product.Drink;
import org.example.learn.сreational.abstractfactory.realization.product.EnergyDrink;
import org.example.learn.сreational.abstractfactory.realization.product.impl.AdrenalineRush;
import org.example.learn.сreational.abstractfactory.realization.product.impl.Pepsi;

public class PepsiFactory implements AbstractFactory {

    @Override
    public Drink createDrink() {
        System.out.println("Create new drink: Pepsi");
        return new Pepsi();
    }

    @Override
    public EnergyDrink createEnergyDrink() {
        System.out.println("Create new drink: AdrenalineRush");
        return new AdrenalineRush();
    }
}
