package org.example.learn.сreational.multiton.realization;

public class Factory {

    private final FactoryTypeEnum type;

    public Factory(FactoryTypeEnum type) {
        System.out.println("Create factory with type: " + type);
        this.type = type;
    }
}
