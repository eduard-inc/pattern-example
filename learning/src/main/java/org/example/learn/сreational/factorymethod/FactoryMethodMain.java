package org.example.learn.сreational.factorymethod;

import org.example.learn.сreational.factorymethod.realization.Conveyor;
import org.example.learn.сreational.factorymethod.realization.conveyors.ColaConveyor;
import org.example.learn.сreational.factorymethod.realization.conveyors.SodaConveyor;

/**
 * Паттерн FactoryMethod предоставляет интерфейс для создания экземпляров класса,
 *  делегирую наследникам самим опеределять какой класс создавать,
 *  позволяя работать с объектами на более асбтракном уровне
 */
public class FactoryMethodMain {
    public static void main(String[] args) {
        Conveyor colaConveyor = new ColaConveyor();
        System.out.println("Cola conveyor create: " + colaConveyor.createDrink().getClass().getSimpleName());

        Conveyor sodaConveyor = new SodaConveyor();
        System.out.println("Soda conveyor create: " + sodaConveyor.createDrink().getClass().getSimpleName());
    }
}
