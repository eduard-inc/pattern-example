package org.example.learn.сreational.prototype.realization;

import org.example.learn.сreational.objectpool.realization.Bottle;

public interface PrototypeBottle {

    public PrototypeBottle copy();

}
