package org.example.learn.сreational.lazyinitialization;

import org.example.learn.сreational.lazyinitialization.realization.Factory;

/**
 *  Паттерн Lazy initialization шаблон порождения объекта при первом обращении к нему
 *  , а не за ранее, облегчая
 *
 *  Реализуется ленивым прокси, легковесом т.д.
 *
 */
public class LazyInitializationMain {
    public static void main(String[] args) {
        Factory factory = new Factory();
        factory.getDrink();
        factory.getDrink();
    }
}
