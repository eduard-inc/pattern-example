package org.example.learn.сreational.prototype;

import org.example.learn.сreational.prototype.realization.Bottle;
import org.example.learn.сreational.prototype.realization.Material;
import org.example.learn.сreational.prototype.realization.PrototypeBottle;

/**
 * Паттерн Prototype создает объект путем делегирования задачи, самому объекту.
 */
public class PrototypeMain {
    public static void main(String[] args) {

        PrototypeBottle bottle = new Bottle(Material.GLASS);
        PrototypeBottle bottle1 = bottle.copy();
        PrototypeBottle bottle2 = bottle1.copy();

        System.out.println(bottle.equals(bottle2));

    }
}
