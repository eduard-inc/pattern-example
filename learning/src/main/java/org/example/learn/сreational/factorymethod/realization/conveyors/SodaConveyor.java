package org.example.learn.сreational.factorymethod.realization.conveyors;

import org.example.learn.сreational.factorymethod.realization.Conveyor;
import org.example.learn.сreational.factorymethod.realization.Drink;
import org.example.learn.сreational.factorymethod.realization.drinks.Soda;

public class SodaConveyor extends Conveyor {
    @Override
    public Drink createDrink() {
        return new Soda();
    }
}
