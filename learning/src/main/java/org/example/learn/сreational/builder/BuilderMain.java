package org.example.learn.сreational.builder;

import org.example.learn.сreational.builder.realization.Drink;

/**
 * Парттер Builder, предназначен для создания сложных объектов используя дополнительный (вложенный класс)
 *
 * Реализован аннотацией @Builder в lombok
 */
public class BuilderMain {
    public static void main(String[] args) {
        Drink drink = Drink.builder()
                .title("Coca-Cola")
                .producer("The Coca-Cola Company")
                .volume(2.0)
                .build();

        System.out.println(drink);
    }
}
