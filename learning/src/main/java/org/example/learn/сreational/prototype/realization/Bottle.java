package org.example.learn.сreational.prototype.realization;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class Bottle implements PrototypeBottle {

    private final Material material;

    public Bottle (Material material) {
        System.out.println("Created new bottle from: " + material);
        this.material = material;
    }

    @Override
    public PrototypeBottle copy() {
        System.out.println("Generated bottle by copying");
        return new Bottle(this.material);
    }
}
