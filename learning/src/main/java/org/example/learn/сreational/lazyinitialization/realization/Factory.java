package org.example.learn.сreational.lazyinitialization.realization;

public class Factory {

    private Drink drink;

    public Factory() {
        System.out.println("Drink is: " + drink);
    }

    public Drink getDrink() {
        if (drink == null) {
            System.out.println("Create drink");
            drink = new Drink();
        }
        return drink;
    }
}
