package org.example.learn.сreational.singleton.realization;

public class SingletonExample {

    private static SingletonExample instance;

    private SingletonExample() {
        System.out.println("Create singleton instance");
    }

    public static synchronized SingletonExample getInstance() {
        if (instance == null) {
            instance = new SingletonExample();
        } else {
            System.out.println("Get from instance");
        }
        return instance;
    }

}
