package org.example.learn.сreational.singleton;

import org.example.learn.сreational.singleton.realization.SingletonExample;

/**
 * Паттерн Singleton гарантирует, что будет создан только один экземпляр некоторого класса
 */
public class SingletonMain {
    public static void main(String[] args) {
        SingletonExample.getInstance();
        SingletonExample.getInstance();
        SingletonExample.getInstance();
    }
}
