package org.example.learn.сreational.abstractfactory;

import org.example.learn.сreational.abstractfactory.realization.conveyor.AbstractFactory;
import org.example.learn.сreational.abstractfactory.realization.conveyor.ColaFactory;
import org.example.learn.сreational.abstractfactory.realization.conveyor.PepsiFactory;

/**
 * Паттерн Abstract Factory, призван помочь в создании различных семейст продуктов.
 *
 */
public class AbstractFactoryMain {
    public static void main(String[] args) {

        AbstractFactory conveyorCola = new ColaFactory();
        conveyorCola.createDrink();
        conveyorCola.createEnergyDrink();

        AbstractFactory conveyorPepsi = new PepsiFactory();
        conveyorPepsi.createDrink();
        conveyorCola.createEnergyDrink();

    }
}
