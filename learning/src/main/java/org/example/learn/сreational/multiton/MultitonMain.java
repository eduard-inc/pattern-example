package org.example.learn.сreational.multiton;

import org.example.learn.сreational.multiton.realization.FactoryTypeEnum;
import org.example.learn.сreational.multiton.realization.Multiton;

/**
 * Паттерн Multiton, упраялет созданием Singleton'ами через именновый массив (hash map)
 *
 */
public class MultitonMain {
    public static void main(String[] args) {
        Multiton multiton = new Multiton();
        multiton.getFactory(FactoryTypeEnum.COLA);
        multiton.getFactory(FactoryTypeEnum.SODA);
        multiton.getFactory(FactoryTypeEnum.SODA);
        multiton.getFactory(FactoryTypeEnum.COLA);
        multiton.getFactory(FactoryTypeEnum.COLA);
    }
}
