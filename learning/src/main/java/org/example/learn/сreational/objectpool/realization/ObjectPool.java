package org.example.learn.сreational.objectpool.realization;

import java.util.LinkedList;
import java.util.Queue;

public class ObjectPool {

    private Queue<Bottle> freeList = new LinkedList<>();
    private Queue<Bottle> busyList = new LinkedList<>();

    public synchronized Bottle get() {
        Bottle bottle;
        if (freeList.isEmpty()){
            bottle = new Bottle();
            System.out.println("created new bottle");
        } else {
            bottle = freeList.poll();
            System.out.println("get bottle from pool");
        }

        busyList.add(bottle);
        return bottle;
    }

    public synchronized void remove(Bottle bottle){
        System.out.println("return bottle to pool");
        busyList.remove(bottle);
        freeList.add(bottle);
    }

}
