package org.example.learn.structural.composite.realization.nirvana;

import org.example.learn.structural.composite.realization.Artist;

public class KristNovoselic implements Artist {

    @Override
    public void printArtistName() {
        System.out.println(getClass().getSimpleName());
    }
}
