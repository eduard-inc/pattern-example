package org.example.learn.structural.bridge;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public abstract class Drink {
    protected BottleMaterial bottleMaterial;

    public abstract void fillBottle();

}
