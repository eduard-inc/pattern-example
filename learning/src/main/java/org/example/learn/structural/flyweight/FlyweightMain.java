package org.example.learn.structural.flyweight;

import org.example.learn.structural.flyweight.realization.Sample;
import org.example.learn.structural.flyweight.realization.SampleFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Паттерн Flyweight позволяет не создавать лишние объекты необходимые в разных частях программы,
 *  и позволяем переиспльзовать уже существующие
 *
 *  Сама концепция очень похожа на КЕШ, с условием неизменяемости самого объекта.
 */
public class FlyweightMain {
    public static void main(String[] args) {


        List<Sample> sampleList = new ArrayList<>();

        sampleList.add(SampleFactory.createSample("control"));
        sampleList.add(SampleFactory.createSample("control"));
        sampleList.add(SampleFactory.createSample("control"));
        sampleList.add(SampleFactory.createSample("standard"));
        sampleList.add(SampleFactory.createSample("standard"));
        sampleList.add(SampleFactory.createSample("standard"));

        sampleList.forEach(Sample::checkSample);
    }
}
