package org.example.learn.structural.bridge;

public interface BottleMaterial {

    void pourInto();

}
