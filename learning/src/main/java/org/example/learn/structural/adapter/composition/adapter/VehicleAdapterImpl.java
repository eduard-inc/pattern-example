package org.example.learn.structural.adapter.composition.adapter;

import lombok.AllArgsConstructor;
import org.example.learn.structural.adapter.composition.Vehicle;
import org.example.learn.structural.adapter.composition.VehicleAdapter;

@AllArgsConstructor
public class VehicleAdapterImpl implements VehicleAdapter {

    private Vehicle car;

    @Override
    public double getMaxSpeed() {
        return car.getMaxSpeed() / 1.609344;
    }

}
