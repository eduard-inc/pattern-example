package org.example.learn.structural.adapter.composition;

public interface Vehicle {
    /**
     *  return speed in KM/H
     */
    double getMaxSpeed();
}
