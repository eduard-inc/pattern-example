package org.example.learn.structural.adapter.extend;

public interface BottleMeter {

    double getVolume();

}
