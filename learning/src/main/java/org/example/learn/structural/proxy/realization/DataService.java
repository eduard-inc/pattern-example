package org.example.learn.structural.proxy.realization;

public interface DataService {

    void checkData();

    void updateData();

}
