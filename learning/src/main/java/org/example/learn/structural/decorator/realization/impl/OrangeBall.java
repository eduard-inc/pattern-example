package org.example.learn.structural.decorator.realization.impl;

import org.example.learn.structural.decorator.realization.Ball;

public class OrangeBall implements Ball {
    @Override
    public void roll() {
        System.out.println(getClass().getSimpleName() + "  is rolling");
    }
}
