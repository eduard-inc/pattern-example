package org.example.learn.structural.adapter.extend;

public interface WeightMeter {

    double getWeight();

}
