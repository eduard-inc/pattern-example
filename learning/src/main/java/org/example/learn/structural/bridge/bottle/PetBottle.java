package org.example.learn.structural.bridge.bottle;

import org.example.learn.structural.bridge.BottleMaterial;

public class PetBottle implements BottleMaterial {
    @Override
    public void pourInto() {
        System.out.println("Pet bottle is full");
    }
}
