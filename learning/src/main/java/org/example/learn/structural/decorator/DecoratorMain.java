package org.example.learn.structural.decorator;

import org.example.learn.structural.decorator.realization.Ball;
import org.example.learn.structural.decorator.realization.impl.BasketballBall;
import org.example.learn.structural.decorator.realization.impl.OrangeBall;

/**
 *  Паттерн декоратор, создан для возможности расширения функциональности существующего объекта без его изменения.
 *  Классы декораторы - оборачивают исходный класс (посредством композиции) и добаляет новую функциональность объекту
 */
public class DecoratorMain {
    public static void main(String[] args) {

        Ball orangeBall = new OrangeBall();
        orangeBall.roll();

        Ball basketballBall = new BasketballBall(new OrangeBall());
        basketballBall.roll();

    }
}
