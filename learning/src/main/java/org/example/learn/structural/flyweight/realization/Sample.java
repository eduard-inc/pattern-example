package org.example.learn.structural.flyweight.realization;

public interface Sample {

    void checkSample();

}
