package org.example.learn.structural.adapter.composition.impl;

import org.example.learn.structural.adapter.composition.Vehicle;

public class FordMondeo implements Vehicle {

    @Override
    public double getMaxSpeed() {
        return 218;
    }
}
