package org.example.learn.structural.decorator.realization.impl;

import org.example.learn.structural.decorator.realization.Ball;
import org.example.learn.structural.decorator.realization.BallDecorator;


public class BasketballBall extends BallDecorator {

    public BasketballBall(Ball ball) {
        super(ball);
    }

    @Override
    public void roll() {
        rollAndJump();
    }

    public void rollAndJump(){
        System.out.println(getClass().getSimpleName() + "  is rolling and jumping");
    }
}
