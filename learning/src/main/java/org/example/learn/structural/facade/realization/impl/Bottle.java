package org.example.learn.structural.facade.realization.impl;

public class Bottle {
    public void get() {
        System.out.println("Get the bottle");
    }

    public void removeCover() {
        System.out.println("Remove the cover");
    }

    public void pour(Water water) {
        System.out.println("Pour " + water.getClass().getSimpleName());
    }

    public void closeCover() {
        System.out.println("Close the cover");
    }
}
