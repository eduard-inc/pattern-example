package org.example.learn.structural.flyweight.realization.impl;

import org.example.learn.structural.flyweight.realization.Sample;

public class ControlSample implements Sample {

    public ControlSample() {
        System.out.println("Create: " + getClass().getSimpleName());
    }

    @Override
    public void checkSample() {
        System.out.println("Check: " + getClass().getSimpleName());
    }
}
