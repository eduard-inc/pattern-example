package org.example.learn.structural.bridge.drink;

import org.example.learn.structural.bridge.BottleMaterial;
import org.example.learn.structural.bridge.Drink;

public class Soda extends Drink {
    public Soda(BottleMaterial bottleMaterial) {
        super(bottleMaterial);
    }

    @Override
    public void fillBottle() {
        System.out.println("Bottle is filled with soda");
        bottleMaterial.pourInto();
    }
}
