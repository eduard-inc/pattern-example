package org.example.learn.structural.decorator.realization;

public interface Ball {

    void roll();

}
