package org.example.learn.structural.proxy.realization.impl;

import org.example.learn.structural.proxy.realization.DataService;

public class PrivateDataServiceProxy implements DataService {

    private PrivateDataService privateDataService;

    @Override
    public void checkData() {
        lazyInit();
        privateDataService.checkData();
    }

    @Override
    public void updateData() {
        lazyInit();
        privateDataService.updateData();
    }

    private synchronized void lazyInit() {
        if (privateDataService == null){
            privateDataService = new PrivateDataService();
        }
    }

}
