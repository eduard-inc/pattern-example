package org.example.learn.structural.adapter.extend.adapter;

import org.example.learn.structural.adapter.extend.WeightMeter;
import org.example.learn.structural.adapter.extend.impl.ColaBottle;

public class ColaBottleAdapter extends ColaBottle implements WeightMeter {

    private final double WEIGHT_MULTIPLIER = 1.12;

    @Override
    public double getWeight() {
        return getVolume() * WEIGHT_MULTIPLIER;
    }
}
