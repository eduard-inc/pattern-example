package org.example.learn.structural.composite.realization;

public interface Artist {
    void printArtistName();
}
