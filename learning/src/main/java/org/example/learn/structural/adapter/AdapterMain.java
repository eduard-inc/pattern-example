package org.example.learn.structural.adapter;

import org.example.learn.structural.adapter.composition.Vehicle;
import org.example.learn.structural.adapter.composition.VehicleAdapter;
import org.example.learn.structural.adapter.composition.impl.FordMondeo;
import org.example.learn.structural.adapter.composition.adapter.VehicleAdapterImpl;
import org.example.learn.structural.adapter.extend.WeightMeter;
import org.example.learn.structural.adapter.extend.adapter.ColaBottleAdapter;

/**
 * Паттерн адаптер действует как прослойка между двумя несовместимыми интерфейсами.
 * Адаптер оборачивает существующий класс новым интерфейсом, чтобы он страл совместимым с интерфейсом клиента.
 *
 * Включение уже существующего класса в другой класс.
 * Интерфейс включающего класса приводится в соответствие с новыми требованиями,
 * а вызовы его методов преобразуются в вызовы методов включённого класса.
 *
 * Реализуется через наследование или композицию
 */
public class AdapterMain {

    public static void main(String[] args) {
        //Реализация композиции
        Vehicle fordMondeo = new FordMondeo();
        VehicleAdapter fordMondeoAdepter = new VehicleAdapterImpl(fordMondeo);
        System.out.printf("Speed vehicle in %s KM/H equals %s MPH \n",
                fordMondeo.getMaxSpeed(),
                fordMondeoAdepter.getMaxSpeed());

        //Реализация наследования
        ColaBottleAdapter adapter = new ColaBottleAdapter();
        WeightMeter weightMeter = adapter;
        System.out.printf("Weight of a %s ml bottle of cola: %s g \n", adapter.getVolume(), weightMeter.getWeight());
    }

}
