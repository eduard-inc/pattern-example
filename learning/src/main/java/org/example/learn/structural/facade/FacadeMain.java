package org.example.learn.structural.facade;

import org.example.learn.structural.facade.realization.FillBottleFacade;

/**
 * Паттерн фасад, создан для сокрытия сложной системы за простым интерфейсом.
 * Позволяет изменять подсистему не изменяя взаимодействие с клиентом
 */
public class FacadeMain {
    public static void main(String[] args) {
        FillBottleFacade facade = new FillBottleFacade();
        facade.fillBottle();
    }
}
