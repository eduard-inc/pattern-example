package org.example.learn.structural.proxy.realization.impl;

import org.example.learn.structural.proxy.realization.DataService;

public class PrivateDataService implements DataService {
    @Override
    public void checkData() {
        System.out.println("Check data");
    }

    @Override
    public void updateData() {
        System.out.println("Update data");
    }
}
