package org.example.learn.structural.adapter.extend.impl;

import org.example.learn.structural.adapter.extend.BottleMeter;

public class ColaBottle implements BottleMeter {
    @Override
    public double getVolume() {
        //return volume im ml
        return 1000;
    }
}
