package org.example.learn.structural.bridge.bottle;

import org.example.learn.structural.bridge.BottleMaterial;

public class GlassBottle implements BottleMaterial {

    @Override
    public void pourInto() {
        System.out.println("Glass bottle is full");
    }
}
