package org.example.learn.structural.composite;

import org.example.learn.structural.composite.realization.nirvana.DaveGrohl;
import org.example.learn.structural.composite.realization.nirvana.KristNovoselic;
import org.example.learn.structural.composite.realization.nirvana.KurtCobain;
import org.example.learn.structural.composite.realization.nirvana.NirvanaComposite;

/**
 *  Паттерн Компоновщик создан для группировки несокльких объеков в древовидную структу.
 * Позволяет обращаться к отдельным объектам и целым группам одинаково
 */
public class CompositeMain {
    public static void main(String[] args) {

        NirvanaComposite nirvana = new NirvanaComposite();
        nirvana.addArtist(new KurtCobain());
        nirvana.addArtist(new KristNovoselic());
        nirvana.addArtist(new DaveGrohl());

        nirvana.printArtistName();

    }
}
