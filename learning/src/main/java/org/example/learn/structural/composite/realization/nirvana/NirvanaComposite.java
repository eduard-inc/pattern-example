package org.example.learn.structural.composite.realization.nirvana;

import org.example.learn.structural.composite.realization.Artist;

import java.util.ArrayList;
import java.util.List;

public class NirvanaComposite implements Artist{

    List<Artist> artistList = new ArrayList<>();

    public void addArtist(Artist artist){
        artistList.add(artist);
    }

    public void removeArtist(Artist artist){
        artistList.remove(artist);
    }

    @Override
    public void printArtistName() {
        artistList.forEach(Artist::printArtistName);
    }
}
