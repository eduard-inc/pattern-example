package org.example.learn.structural.facade.realization;

import org.example.learn.structural.facade.realization.impl.Bottle;
import org.example.learn.structural.facade.realization.impl.Water;

public class FillBottleFacade {
    private final Water water = new Water();
    private final Bottle bottle = new Bottle();

    public void fillBottle() {

        bottle.get();
        bottle.removeCover();
        bottle.pour(water);
        bottle.closeCover();

    }

}
