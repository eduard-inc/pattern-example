package org.example.learn.structural.bridge;

import org.example.learn.structural.bridge.bottle.GlassBottle;
import org.example.learn.structural.bridge.bottle.PetBottle;
import org.example.learn.structural.bridge.drink.Cola;
import org.example.learn.structural.bridge.drink.Soda;

/**
 *  Паттер Мост создан для отделения абстракции от ее реализации, для ее независимого использования.
 *  т.е. Позволяет абстракции и реализации изменяться независимо друг от друга
 *
 *
 */
public class BridgeMain {

    public static void main(String[] args) {

        Drink drinkCola = new Cola(new GlassBottle());
        drinkCola.fillBottle();

        Drink drinkSoda = new Soda(new PetBottle());
        drinkSoda.fillBottle();

    }

}
