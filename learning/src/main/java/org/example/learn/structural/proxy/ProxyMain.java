package org.example.learn.structural.proxy;

import org.example.learn.structural.proxy.realization.DataService;
import org.example.learn.structural.proxy.realization.impl.PrivateDataServiceProxy;

/**
 * Паттерн Proxy создает класс прослойку, наследующий тот же интерфейс.
 * Позволяет модифицировать вызов объекта без изменения самого объекта.
 *
 * Применяеться для ленивой инициализации, кеширования, логирования и т.д.
 */
public class ProxyMain {
    public static void main(String[] args) {
        DataService dataService = new PrivateDataServiceProxy();
        dataService.checkData();
        dataService.updateData();
    }
}
