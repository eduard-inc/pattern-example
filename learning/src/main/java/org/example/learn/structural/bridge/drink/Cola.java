package org.example.learn.structural.bridge.drink;

import org.example.learn.structural.bridge.BottleMaterial;
import org.example.learn.structural.bridge.Drink;

public class Cola extends Drink {
    public Cola(BottleMaterial bottleMaterial) {
        super(bottleMaterial);
    }

    @Override
    public void fillBottle() {
        System.out.println("Bottle is filled with cola");
        bottleMaterial.pourInto();
    }
}
