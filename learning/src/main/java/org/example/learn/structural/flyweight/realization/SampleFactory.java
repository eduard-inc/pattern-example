package org.example.learn.structural.flyweight.realization;

import org.example.learn.structural.flyweight.realization.impl.ControlSample;
import org.example.learn.structural.flyweight.realization.impl.StandardSample;

import java.util.HashMap;
import java.util.Map;

public class SampleFactory {

    private static Map<String, Sample> sampleCache = new HashMap<>();

    public static Sample createSample(String type) {

        Sample newSample = sampleCache.get(type);

        if (newSample == null) {
            switch (type) {
                case ("control"): {
                    newSample = new ControlSample();
                    break;
                }
                case ("standard"): {
                    newSample = new StandardSample();
                    break;
                }
            }
            sampleCache.put(type, newSample);
        }
        return newSample;
    }

}
