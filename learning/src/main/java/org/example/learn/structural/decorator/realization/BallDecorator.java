package org.example.learn.structural.decorator.realization;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public abstract class BallDecorator implements Ball {

    protected Ball ball;

}
