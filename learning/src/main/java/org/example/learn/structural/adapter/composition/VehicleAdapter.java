package org.example.learn.structural.adapter.composition;

public interface VehicleAdapter{

    /**
     *  return speed in MPH
     */
    double getMaxSpeed();
}
